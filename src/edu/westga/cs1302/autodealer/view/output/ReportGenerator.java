package edu.westga.cs1302.autodealer.view.output;

import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = inventory.getDealershipName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
			Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();
			
			summary += System.lineSeparator();

			summary += "Most expensive auto: ";
			summary += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
			summary += "Least expensive auto: ";
			summary += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
			summary += "Average miles:  ";
			summary += inventory.getAverageMiles() + System.lineSeparator();
			summary += this.getPriceRangeReport(inventory, 3000.00);
			summary += this.getPriceRangeReport(inventory, 10000.00);
			
		}

		return summary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

		String output = currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}
	
	private String getPriceRangeReport(Inventory inventory, Double price) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		int[] list = inventory.numberOfVehiclesByPriceSegment(price);
		String output = "Vehicles in " + currencyFormatter.format(price) + " price range segments:  " + System.lineSeparator();
		
		for (int i = 0; i < list.length; i++) {
			Double upperLimit = (i + 1) * price;
			Double lowerLimit = upperLimit - price;
			
			if (lowerLimit != 0.0) {
				lowerLimit = lowerLimit + 0.01;
			}
			
			output += currencyFormatter.format(lowerLimit) + " - ";
			output += currencyFormatter.format(upperLimit) + " : ";
			output += list[i] + System.lineSeparator();
		}
		
		return output;
	}

}
